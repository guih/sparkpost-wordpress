# SparkPost  - Wordpress v0.1


SparkPost for WordPress is designed to plug-in into WordPress core functions, check it out how you can attach the solution to your WordPress installation;


At first you must upload this code contents to your plugin directory, you can find it in `wp-content/plugins/`. Before you start you must do this.
### Set up

You can add it in any part of theme or plugin in yout WordPress instalation.
Here is how you'll implement it in `functions.php` file of a theme.

Create instance of class


```php
<?php
if(class_exists( 'SprakPostWordPress' ))
	$instance = new SprakPostWordPress();
```

And you're ready to send e-mails as you send with `wp_mail` :)

```sh
<?php
$instance->send($to, $subject, $message, $headers);
```

### Note

You must properly set an send domain in your SparkPost dashboard.

