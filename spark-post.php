<?php
/**
 * Plugin Name: SparkPost Adapter
 * Plugin URI:  https://api.whatsapp.com/send?phone=5533999412183&text=Ol%C3%A1%20Guilherme%2C%20vim%20atrav%C3%A9s%20do%20plugin%20SparkPOst%20para%20WordPress!
 * Description: Este plugin possibilita a integração do seriço SparkPost ao WordPress. Ative o plugin SparkPost e vá para a página Configurações do SprakPostWordPress para configurar sua chave API.
 * Version:     0.1
 * Author:      Guilherme Henrique
 * Author URI:  https://api.whatsapp.com/send?phone=5533999412183&text=Ol%C3%A1%20Guilherme%2C%20vim%20atrav%C3%A9s%20do%20plugin%20SparkPOst%20para%20WordPress!
 */


require 'vendor/autoload.php';
use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use ZBateson\MailMimeParser\Message as MailParse;


Class SprakPostWordPress {

	private $key ='';

	private $client;

	private $sparky;


	public function __construct(){

		$this->key =  get_option('spark-post-api-key');


		if( $this->key !== '' ) :
			$this->client = new GuzzleAdapter(new Client());

			$this->sparky = new SparkPost($this->client, array(
				"key" => $this->key
				)
			);				
		else:
			$this->notifySetup();
		endif;
	
		//$this->buildSettings();
	}

	public function send($to, $subject='', $text='', $headers){

		if( $this->key == '' || !$this->key ) :
			wp_mail($to, $subject, $text, $headers);
		else :
			$message = $this->legacyParseHeaders( $headers );

			
			$action_send = $this->sparky->transmissions->post([
			    'content' => [
			        'from' => [
			            'name' => $message->getHeader('from')->getPersonName(),
			            'email' => $message->getHeaderValue('from'),
			        ],
			        'subject' => $subject,
			        'html' => $text,
			        // 'text' => $args['content_raw'],
			    ],
			    'recipients' => [
			        [
			            'address' => [
			                'name' => $message->getHeader('to')->getPersonName(),
			                'email' => $message->getHeaderValue('to'),
			            ],
			        ],
			    ],
			]);

			try {
			    $response = $action_send->wait();
			    // echo $response->getStatusCode()."\n";
			    // print_r($response->getBody())."\n";
			} catch (\Exception $e) {
			    echo $e->getMessage();
			}					
		endif;

	}

	public function legacyParseHeaders( $headers ){
		return MailParse::from($headers);
	}


	public function notifySetup(){
		add_action('admin_notices', function(){
			echo '<div class="notice notice-error is-dismissible">
				<p><strong>Atenção!</strong> Você deve ajustar o plug-in SparkPost.</p>
			</div>';			
		});
	}

	public function buildSettings(){
		add_action('admin_menu', array( __CLASS__, 'spark_post_settings_page' ) );

	}


	public function spark_post_settings_page() {
			add_menu_page(
				'SparkPost - Configuração', 
				'SparkPost', 
				'administrator', 
				'spark-post-config', 
				array( __CLASS__, 'spark_post_markup' ), 
				'dashicons-email');

			add_action( 'admin_init', array( __CLASS__, 'setup_options' ) );
		}


	public function setup_options() {

		register_setting( 'spark-post-settings', 'spark-post-api-key' );
		register_setting( 'spark-post-settings', 'spark-post-domain' );
	}

	public function spark_post_markup() {
	?>
	<div class="wrap">
		<h1>SparkPost - Configuração</h1>
		<hr>

		<form method="post" action="options.php">
		    <?php settings_fields( 'spark-post-settings' ); ?>
		    <?php do_settings_sections( 'spark-post-settings' ); ?>
		    <table class="form-table">
		        <tr valign="top">
		        <th scope="row">Chave API</th>
		        <td><input type="text" style="width:400px" placeholder="4e5ccd5acdc473772e23578a6513b1957914da728" name="spark-post-api-key" value="<?php echo esc_attr( get_option('spark-post-api-key') ); ?>" /></td>
		        </tr>
		         
		        <tr valign="top">
		        <th scope="row">Domínio</th>
		        <td><input type="text" name="spark-post-domain" placeholder="example.com" value="<?php echo esc_attr( get_option('spark-post-domain') ); ?>" />
		        	<p><strong>Nota</strong> Insira o domínio autorizado no painel SparkPost <a href="https://developers.sparkpost.com/api/sending-domains/" target="_blank">saiba mais</a><br>Caso o domínio do e-mail remetente seja o mesmo da instalação, este campo pode ficar em branco.</p></td>
		        </tr>
		        
		    </table>
		    
		    <?php submit_button('Salvar alterações'); ?>

		</form>
	</div>
	<?php }	
}

add_action( 'plugins_loaded', array( 'SprakPostWordPress', 'buildSettings' ) );



// $instance->send(array(
// 	'from_name' => 'Guilherme',
// 	'from_email' => 'guih@stux.me',
// 	'subject' => 'Assunto',
// 	'content' => 'Conteúdo teste',
// 	'content_raw' => 'Content RAW',
// 	'to_name' => 'Receipt',
// 	'to_email' => 'guihknx@gmail.com'

// ));

    // // To send HTML mail, the Content-type header must be set
    // $headers = 'MIME-Version: 1.0' . "\r\n";
    // $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

    // // Additional headers
    // $headers .= 'To: Guilherme <guihknx@gmail.com>' . "\r\n";
    // $headers .= 'From: ' . 'RifandoNet' . ' <' . 'admin@stux.me' . '>' . "\r\n";
    // echo $hedaders;

//$instance->send('guihknx@gmail.com','XXXXXXXAssunto Teste novo', 'Meu emaiil', $headers);


?>
